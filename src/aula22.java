public class aula22 {  //looping

    public static void main(String[] args) {

        int x = 0;  //valor 0
        while (x <= 10){  // x menor ou igual 10
            x = x - 1;  //  valor sempre -1 , looping eterno
            System.out.println( x ); //imprimir valor x , sempre valor negativo nunca chegando a 10
        }

    }
}
